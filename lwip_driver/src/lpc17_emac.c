/**********************************************************************
 * $Id$         lpc17_emac.c                    2011-11-20
 *
 * @file             lpc17_emac.c
 * @brief    LPC17 ethernet driver for LWIP
 * @version  1.0
 * @date             20. Nov. 2011
 * @author   NXP MCU SW Application Team
 *
 * Copyright(C) 2011, NXP Semiconductor
 * All rights reserved.
 *
 ***********************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
 **********************************************************************/

#include "lpc17xx_emac_arch.h"
#include "lwip/debug.h"
#include "lwip/sys.h"
#include "lwip/netif.h"
#include "lwip/stats.h"
#include "netif/etharp.h"
#include "lpc17xx_emac.h"
#include "lpc_emac_config.h"
#include "lpc_phy.h"
#include "bsp/irq.h"
#include <string.h>
#include <signal.h>

#include <semaphore.h>
#include <unistd.h>
#include "LPC17xx.h"
#define ALIGNED(n) __attribute__((aligned(n)))

#ifndef LPC_EMAC_RMII
#error LPC_EMAC_RMII is not defined!
#endif

#if LPC_NUM_BUFF_TXDESCS < 2
#error LPC_NUM_BUFF_TXDESCS must be at least 2
#endif

#if LPC_NUM_BUFF_RXDESCS < 3
#error LPC_NUM_BUFF_RXDESCS must be at least 3
#endif

/** \brief  Driver transmit and receive thread priorities
 *
 * Thread priorities for receive thread and TX cleanup thread. Alter
 * to prioritize receive or transmit bandwidth. In a heavily loaded
 * system or with LEIP_DEBUG enabled, the priorities might be better
 * the same. */
#define RX_PRIORITY   (0)
#define TX_PRIORITY   (0)

/** \brief  Receive group interrupts
 */
#define RXINTGROUP (EMAC_INT_RX_OVERRUN | EMAC_INT_RX_ERR | EMAC_INT_RX_DONE)

/** \brief  Transmit group interrupts
 */
#define TXINTGROUP (EMAC_INT_TX_UNDERRUN | EMAC_INT_TX_ERR | EMAC_INT_TX_DONE)

/** \brief  Signal used for ethernet ISR to signal packet_rx() thread.
 */
#define RX_SIGNAL  1

/** \brief  Structure of a TX/RX descriptor
 */
typedef struct
{
    volatile u32_t packet;        /**< Pointer to buffer */
    volatile u32_t control;       /**< Control word */
} LPC_TXRX_DESC_T;

/** \brief  Structure of a RX status entry
 */
typedef struct
{
    volatile u32_t statusinfo;   /**< RX status word */
    volatile u32_t statushashcrc; /**< RX hash CRC */
} LPC_TXRX_STATUS_T;

/* LPC EMAC driver data structure */
struct lpc_enetdata {
    /* prxs must be 8 byte aligned! */
    LPC_TXRX_STATUS_T prxs[LPC_NUM_BUFF_RXDESCS]; /**< Pointer to RX statuses */
    struct netif *netif;        /**< Reference back to LWIP parent netif */
    LPC_TXRX_DESC_T ptxd[LPC_NUM_BUFF_TXDESCS];   /**< Pointer to TX descriptor list */
    LPC_TXRX_STATUS_T ptxs[LPC_NUM_BUFF_TXDESCS]; /**< Pointer to TX statuses */
    LPC_TXRX_DESC_T prxd[LPC_NUM_BUFF_RXDESCS];   /**< Pointer to RX descriptor list */
    struct pbuf *rxb[LPC_NUM_BUFF_RXDESCS]; /**< RX pbuf pointer list, zero-copy mode */
    u32_t rx_fill_desc_index; /**< RX descriptor next available index */
    volatile u32_t rx_free_descs; /**< Count of free RX descriptors */
    struct pbuf *txb[LPC_NUM_BUFF_TXDESCS]; /**< TX pbuf pointer list, zero-copy mode */
    u32_t lpc_last_tx_idx; /**< TX last descriptor index, zero-copy mode */
    sys_thread_t RxThread; /**< RX receive thread data object pointer */
    sys_thread_t TxThread; /**< TX transmit thread data object pointer */
    sem_t TxCleanSem; /**< TX cleanup thread wakeup semaphore */
    sys_mutex_t TXLockMutex; /**< TX critical section mutex */
    sys_sem_t xTXDCountSem; /**< TX free buffer counting semaphore */
    sigset_t rxset;
};

#if LWIP_USE_ETHMEM_LINKER_SECTION
#define ETHMEM_SECTION __attribute__((section(LWIP_ETHMEM_LINKER_SECTION_NAME),aligned))
#else /* LWIP_USE_ETHMEM_LINKER_SECTION */
#define ETHMEM_SECTION ALIGNED(8)
#endif /* LWIP_USE_ETHMEM_LINKER_SECTION */

/** \brief  LPC EMAC driver work data
 */
ETHMEM_SECTION struct lpc_enetdata lpc_enetdata;


static void phy_update(void* args);
#ifdef DUMP_DRIVER_INFO
static void dump_driver_info(void);
#endif /* DUMP_DRIVER_INFO */



/** \brief  Queues a pbuf into the RX descriptor list
 *
 *  \param[in] lpc_enetif Pointer to the drvier data structure
 *  \param[in] p            Pointer to pbuf to queue
 */
static void lpc_rxqueue_pbuf(struct lpc_enetdata *lpc_enetif, struct pbuf *p)
{
    u32_t idx;

    /* Get next free descriptor index */
    idx = lpc_enetif->rx_fill_desc_index;

    /* Setup descriptor and clear statuses */
    lpc_enetif->prxd[idx].control = EMAC_RCTRL_INT | ((u32_t) (p->len - 1));
    lpc_enetif->prxd[idx].packet = (u32_t) p->payload;
    lpc_enetif->prxs[idx].statusinfo = 0xFFFFFFFF;
    lpc_enetif->prxs[idx].statushashcrc = 0xFFFFFFFF;

    /* Save pbuf pointer for push to network layer later */
    lpc_enetif->rxb[idx] = p;

    /* Wrap at end of descriptor list */
    idx++;
    if (idx >= LPC_NUM_BUFF_RXDESCS)
        idx = 0;

    /* Queue descriptor(s) */
    lpc_enetif->rx_free_descs -= 1;
    lpc_enetif->rx_fill_desc_index = idx;
    LPC_EMAC->RxConsumeIndex = idx;

    LWIP_DEBUGF(DRIVER_DEBUG,
                ("lpc_rxqueue_pbuf: pbuf packet queued: %p (free desc=%d)\n",
                 (void*)p, lpc_enetif->rx_free_descs));
}

/** \brief  Attempt to allocate and requeue a new pbuf for RX
 *
 *  \param[in]     netif Pointer to the netif structure
 *  \returns         1 if a packet was allocated and requeued, otherwise 0
 */
s32_t lpc_rx_queue(struct netif *netif)
{
    struct lpc_enetdata *lpc_enetif = netif->state;
    struct pbuf *p;
    s32_t queued = 0;

    /* Attempt to requeue as many packets as possible */
    while (lpc_enetif->rx_free_descs > 0) {
        /* Allocate a pbuf from the pool. We need to allocate at the
           maximum size as we don't know the size of the yet to be
           received packet. */
        p = pbuf_alloc(PBUF_RAW, (u16_t) EMAC_ETH_MAX_FLEN, PBUF_RAM);
        if (p == NULL) {
            LWIP_DEBUGF(DRIVER_DEBUG,
                        ("lpc_rx_queue: could not allocate RX pbuf "
                         "(free desc=%d, allocated=%d)\n",
                         lpc_enetif->rx_free_descs, queued));
            return queued;
        }

        /* pbufs allocated from the RAM pool should be non-chained. */
        LWIP_ASSERT("lpc_rx_queue: pbuf is not contiguous (chained)",
                    pbuf_clen(p) <= 1);

        /* Queue packet */
        lpc_rxqueue_pbuf(lpc_enetif, p);

        /* Update queued count */
        queued++;
    }

    return queued;
}

/** \brief  Sets up the RX descriptor ring buffers.
 *
 *  This function sets up the descriptor list used for receive packets.
 *
 *  \param[in]  lpc_enetif  Pointer to driver data structure
 *  \returns                   Always returns ERR_OK
 */
static err_t lpc_rx_setup(struct lpc_enetdata *lpc_enetif)
{
    /* Setup pointers to RX structures */
    LPC_EMAC->RxDescriptor = (u32_t) &lpc_enetif->prxd[0];
    LPC_EMAC->RxStatus = (u32_t) &lpc_enetif->prxs[0];
    LPC_EMAC->RxDescriptorNumber = LPC_NUM_BUFF_RXDESCS - 1;

    lpc_enetif->rx_free_descs = LPC_NUM_BUFF_RXDESCS;
    lpc_enetif->rx_fill_desc_index = 0;

    /* Build RX buffer and descriptors */
    lpc_rx_queue(lpc_enetif->netif);

    return ERR_OK;
}

/** \brief  Allocates a pbuf and returns the data from the incoming packet.
 *
 *  \param[in] netif the lwip network interface structure for this lpc_enetif
 *  \return a pbuf filled with the received packet (including MAC header)
 *         NULL on memory error
 */
static struct pbuf *lpc_low_level_input(struct netif *netif)
{
    struct lpc_enetdata *lpc_enetif = netif->state;
    struct pbuf *p = NULL;
    u32_t idx, length;
    u16_t origLength;

    /* Monitor RX overrun status. This should never happen unless
       (possibly) the internal bus is behing held up by something.
       Unless your system is running at a very low clock speed or
       there are possibilities that the internal buses may be held
       up for a long time, this can probably safely be removed. */
    if (LPC_EMAC->IntStatus & EMAC_INT_RX_OVERRUN) {
        LINK_STATS_INC(link.err);
        LINK_STATS_INC(link.drop);

        /* Temporarily disable RX */
        LPC_EMAC->MAC1 &= ~EMAC_MAC1_REC_EN;

        /* Reset the RX side */
        LPC_EMAC->MAC1 |= EMAC_MAC1_RES_RX;
        LPC_EMAC->IntClear = EMAC_INT_RX_OVERRUN;

        /* De-allocate all queued RX pbufs */
        for (idx = 0; idx < LPC_NUM_BUFF_RXDESCS; idx++) {
            if (lpc_enetif->rxb[idx] != NULL) {
                pbuf_free(lpc_enetif->rxb[idx]);
                lpc_enetif->rxb[idx] = NULL;
            }
        }

        /* Start RX side again */
        lpc_rx_setup(lpc_enetif);

        /* Re-enable RX */
        LPC_EMAC->MAC1 |= EMAC_MAC1_REC_EN;

        return NULL;
    }

    /* Determine if a frame has been received */
    length = 0;
    idx = LPC_EMAC->RxConsumeIndex;
    if (LPC_EMAC->RxProduceIndex != idx) {
        /* Handle errors */
        if (lpc_enetif->prxs[idx].statusinfo & (EMAC_RINFO_CRC_ERR |
                                                EMAC_RINFO_SYM_ERR | EMAC_RINFO_ALIGN_ERR | EMAC_RINFO_LEN_ERR)) {
#if LINK_STATS
            if (lpc_enetif->prxs[idx].statusinfo & (EMAC_RINFO_CRC_ERR |
                                                    EMAC_RINFO_SYM_ERR | EMAC_RINFO_ALIGN_ERR))
                LINK_STATS_INC(link.chkerr);
            if (lpc_enetif->prxs[idx].statusinfo & EMAC_RINFO_LEN_ERR)
                LINK_STATS_INC(link.lenerr);
#endif

            /* Drop the frame */
            LINK_STATS_INC(link.drop);

            /* Re-queue the pbuf for receive */
            lpc_enetif->rx_free_descs++;
            p = lpc_enetif->rxb[idx];
            lpc_enetif->rxb[idx] = NULL;
            lpc_rxqueue_pbuf(lpc_enetif, p);

            LWIP_DEBUGF(DRIVER_DEBUG,
                        ("lpc_low_level_input: Packet dropped with errors (0x%x)\n",
                         lpc_enetif->prxs[idx].statusinfo));

            p = NULL;
        } else {
            /* A packet is waiting, get length */
            length = (lpc_enetif->prxs[idx].statusinfo & 0x7FF) + 1;

            /* Zero-copy */
            p = lpc_enetif->rxb[idx];
            origLength = p->len;
            p->len = (u16_t) length;

            /* Free pbuf from descriptor */
            lpc_enetif->rxb[idx] = NULL;
            lpc_enetif->rx_free_descs++;

            /* Attempt to queue new buffer(s) */
            if (lpc_rx_queue(lpc_enetif->netif) == 0) {
                /* Drop the frame due to OOM. */
                LINK_STATS_INC(link.drop);

                /* Re-queue the pbuf for receive */
                p->len = origLength;
                lpc_rxqueue_pbuf(lpc_enetif, p);

                LWIP_DEBUGF(DRIVER_DEBUG,
                            ("lpc_low_level_input: Packet index %d dropped for OOM\n",
                             idx));

                return NULL;
            }

            LWIP_DEBUGF(DRIVER_DEBUG,
                        ("lpc_low_level_input: Packet received: %p, size %d (index=%d)\n",
                         (void*)p, length, idx));

            /* Save size */
            p->tot_len = (u16_t) length;
            LINK_STATS_INC(link.recv);
        }
    }

    return p;
}

/** \brief  Attempt to read a packet from the EMAC interface.
 *
 *  \param[in] netif the lwip network interface structure for this lpc_enetif
 */
void lpc_enetif_input(struct netif *netif)
{
    struct eth_hdr *ethhdr;
    struct pbuf *p;

    /* move received packet into a new pbuf */
    p = lpc_low_level_input(netif);
    if (p == NULL)
        return;

    /* points to packet payload, which starts with an Ethernet header */
    ethhdr = p->payload;
    LWIP_DEBUGF(DRIVER_DEBUG, ("lpc_enetif_input: packet recveived: ethhdr->type=%d\n", htons(ethhdr->type)));

    switch (htons(ethhdr->type)) {
    case ETHTYPE_IP:
    case ETHTYPE_ARP:
#if PPPOE_SUPPORT
    case ETHTYPE_PPPOEDISC:
    case ETHTYPE_PPPOE:
#endif /* PPPOE_SUPPORT */
        /* full packet send to tcpip_thread to process */
        if (netif->input(p, netif) != ERR_OK) {
            LWIP_DEBUGF(DRIVER_DEBUG, ("lpc_enetif_input: IP input error\n"));
            /* Free buffer */
            pbuf_free(p);
        }
        break;

    default:
        /* Return buffer */
        pbuf_free(p);
        break;
    }
}

/** \brief  Determine if the passed address is usable for the ethernet
 *          DMA controller.
 *
 *  \param[in] addr Address of packet to check for DMA safe operation
 *  \return          1 if the packet address is not safe, otherwise 0
 */
static s32_t lpc_packet_addr_notsafe(void *addr) {
    /* Check for legal address ranges */
    if ((((u32_t) addr >= 0x2007C000) && ((u32_t) addr < 0x20083FFF))) {
        return 0;
    }
    return 1;
}

/** \brief  Sets up the TX descriptor ring buffers.
 *
 *  This function sets up the descriptor list used for transmit packets.
 *
 *  \param[in]      lpc_enetif  Pointer to driver data structure
 */
static err_t lpc_tx_setup(struct lpc_enetdata *lpc_enetif)
{
    s32_t idx;

    /* Build TX descriptors for local buffers */
    for (idx = 0; idx < LPC_NUM_BUFF_TXDESCS; idx++) {
        lpc_enetif->ptxd[idx].control = 0;
        lpc_enetif->ptxs[idx].statusinfo = 0xFFFFFFFF;
    }

    /* Setup pointers to TX structures */
    LPC_EMAC->TxDescriptor = (u32_t) &lpc_enetif->ptxd[0];
    LPC_EMAC->TxStatus = (u32_t) &lpc_enetif->ptxs[0];
    LPC_EMAC->TxDescriptorNumber = LPC_NUM_BUFF_TXDESCS - 1;

    lpc_enetif->lpc_last_tx_idx = 0;

    return ERR_OK;
}

/** \brief  Free TX buffers that are complete
 *
 *  \param[in] lpc_enetif  Pointer to driver data structure
 *  \param[in] cidx  EMAC current descriptor comsumer index
 */
static void lpc_tx_reclaim_st(struct lpc_enetdata *lpc_enetif, u32_t cidx)
{
    /* Get exclusive access */
    sys_mutex_lock(&lpc_enetif->TXLockMutex);

    while (cidx != lpc_enetif->lpc_last_tx_idx) {
        if (lpc_enetif->txb[lpc_enetif->lpc_last_tx_idx] != NULL) {
            LWIP_DEBUGF(DRIVER_DEBUG,
                        ("lpc_tx_reclaim_st: Freeing packet %p (index %d)\n",
                         (void*)lpc_enetif->txb[lpc_enetif->lpc_last_tx_idx],
                         lpc_enetif->lpc_last_tx_idx));
            pbuf_free(lpc_enetif->txb[lpc_enetif->lpc_last_tx_idx]);
            lpc_enetif->txb[lpc_enetif->lpc_last_tx_idx] = NULL;
        }

        sys_sem_signal(&lpc_enetif->xTXDCountSem);
        lpc_enetif->lpc_last_tx_idx++;
        if (lpc_enetif->lpc_last_tx_idx >= LPC_NUM_BUFF_TXDESCS)
            lpc_enetif->lpc_last_tx_idx = 0;
    }

    /* Restore access */
    sys_mutex_unlock(&lpc_enetif->TXLockMutex);
}

/** \brief  User call for freeingTX buffers that are complete
 *
 *  \param[in] netif the lwip network interface structure for this lpc_enetif
 */
void lpc_tx_reclaim(struct netif *netif)
{
    lpc_tx_reclaim_st((struct lpc_enetdata *) netif->state,
                      LPC_EMAC->TxConsumeIndex);
}

/** \brief  Polls if an available TX descriptor is ready. Can be used to
 *           determine if the low level transmit function will block.
 *
 *  \param[in] netif the lwip network interface structure for this lpc_enetif
 *  \return 0 if no descriptors are read, or >0
 */
s32_t lpc_tx_ready(struct netif *netif)
{
    s32_t fb;
    u32_t idx, cidx;
    (void)netif;

    cidx = LPC_EMAC->TxConsumeIndex;
    idx = LPC_EMAC->TxProduceIndex;

    /* Determine number of free buffers */
    if (idx == cidx)
        fb = LPC_NUM_BUFF_TXDESCS;
    else if (cidx > idx)
        fb = (LPC_NUM_BUFF_TXDESCS - 1) -
            ((idx + LPC_NUM_BUFF_TXDESCS) - cidx);
    else
        fb = (LPC_NUM_BUFF_TXDESCS - 1) - (cidx - idx);

    return fb;
}

/** \brief  Low level output of a packet. Never call this from an
 *          interrupt context, as it may block until TX descriptors
 *          become available.
 *
 *  \param[in] netif the lwip network interface structure for this lpc_enetif
 *  \param[in] p the MAC packet to send (e.g. IP packet including MAC addresses and type)
 *  \return ERR_OK if the packet could be sent or an err_t value if the packet couldn't be sent
 */
static err_t lpc_low_level_output(struct netif *netif, struct pbuf *p)
{
    struct lpc_enetdata *lpc_enetif = netif->state;
    struct pbuf *q;
    u8_t *dst;
    u32_t idx, notdmasafe = 0;
    struct pbuf *np;
    s32_t dn;

    /* Zero-copy TX buffers may be fragmented across mutliple payload
       chains. Determine the number of descriptors needed for the
       transfer. The pbuf chaining can be a mess! */
    dn = (s32_t) pbuf_clen(p);

    /* Test to make sure packet addresses are DMA safe. A DMA safe
       address is once that uses external memory or periphheral RAM.
       IRAM and FLASH are not safe! */
    for (q = p; q != NULL; q = q->next)
        notdmasafe += lpc_packet_addr_notsafe(q->payload);

#if LPC_TX_PBUF_BOUNCE_EN==1
    /* If the pbuf is not DMA safe, a new bounce buffer (pbuf) will be
       created that will be used instead. This requires an copy from the
       non-safe DMA region to the new pbuf */
    if (notdmasafe) {
        /* Allocate a pbuf in DMA memory */
        np = pbuf_alloc(PBUF_RAW, p->tot_len, PBUF_RAM);
        if (np == NULL)
            return ERR_MEM;

        /* This buffer better be contiguous! */
        LWIP_ASSERT("lpc_low_level_output: New transmit pbuf is chained",
                    (pbuf_clen(np) == 1));

        /* Copy to DMA safe pbuf */
        dst = (u8_t *) np->payload;
        for(q = p; q != NULL; q = q->next) {
            /* Copy the buffer to the descriptor's buffer */
            MEMCPY(dst, (u8_t *) q->payload, q->len);
            dst += q->len;
        }
        np->len = p->tot_len;

        LWIP_DEBUGF(DRIVER_DEBUG,
                    ("lpc_low_level_output: Switched to DMA safe buffer, old=%p, new=%p\n",
                     (void*)q, (void*)np));

        /* use the new buffer for descrptor queueing. The original pbuf will
           be de-allocated outsuide this driver. */
        p = np;
        dn = 1;
    }
#else
    if (notdmasafe)
        LWIP_ASSERT("lpc_low_level_output: Not a DMA safe pbuf",
                    (notdmasafe == 0));
#endif

    /* Wait until enough descriptors are available for the transfer. */
    /* THIS WILL BLOCK UNTIL THERE ARE ENOUGH DESCRIPTORS AVAILABLE */
    while (dn > lpc_tx_ready(netif)) {
        sys_arch_sem_wait(&lpc_enetif->xTXDCountSem, 0);
    }

    /* Get free TX buffer index */
    idx = LPC_EMAC->TxProduceIndex;

    /* Get exclusive access */
    sys_mutex_lock(&lpc_enetif->TXLockMutex);

    /* Prevent LWIP from de-allocating this pbuf. The driver will
       free it once it's been transmitted. */
    if (!notdmasafe)
        pbuf_ref(p);

    /* Setup transfers */
    q = p;
    while (dn > 0) {
        dn--;

        /* Only save pointer to free on last descriptor */
        if (dn == 0) {
            /* Save size of packet and signal it's ready */
            lpc_enetif->ptxd[idx].control = (q->len - 1) | EMAC_TCTRL_INT |
                EMAC_TCTRL_LAST;
            lpc_enetif->txb[idx] = p;
        }
        else {
            /* Save size of packet, descriptor is not last */
            lpc_enetif->ptxd[idx].control = (q->len - 1) | EMAC_TCTRL_INT;
            lpc_enetif->txb[idx] = NULL;
        }

        LWIP_DEBUGF(DRIVER_DEBUG,
                    ("lpc_low_level_output: pbuf packet(%p) sent, chain#=%d,"
                     " size = %d (index=%d)\n", q->payload, dn, q->len, idx));

        lpc_enetif->ptxd[idx].packet = (u32_t) q->payload;

        q = q->next;

        idx++;
        if (idx >= LPC_NUM_BUFF_TXDESCS)
            idx = 0;
    }

    LPC_EMAC->TxProduceIndex = idx;

    LINK_STATS_INC(link.xmit);

    /* Restore access */
    sys_mutex_unlock(&lpc_enetif->TXLockMutex);

    return ERR_OK;
}

/** \brief  LPC EMAC interrupt handler.
 *
 *  This function handles the transmit, receive, and error interrupt of
 *  the LPC177x_8x.
 */
void ENET_IRQHandler(void* args)
{
    uint32_t ints;

    (void)args;

    /* Interrupts are of 2 groups - transmit or receive. Based on the
       interrupt, kick off the receive or transmit (cleanup) task */

    /* Get pending interrupts */
    ints = LPC_EMAC->IntStatus;

    if (ints & RXINTGROUP) {
        /* RX group interrupt(s): Give signal to wakeup RX receive task.*/
        sys_thread_signal(lpc_enetdata.RxThread, RX_SIGNAL);
    }

    if (ints & TXINTGROUP) {
        /* TX group interrupt(s): Give semaphore to wakeup TX cleanup task. */
        sem_post(&lpc_enetdata.TxCleanSem);
    }

    /* Clear pending interrupts */
    LPC_EMAC->IntClear = ints;
}

/** \brief  Packet reception task
 *
 * This task is called when a packet is received. It will
 * pass the packet to the LWIP core.
 *
 *  \param[in] pvParameters Not used yet
 */
static void packet_rx(void* pvParameters) {
    struct lpc_enetdata *lpc_enetif = pvParameters;
    int sig;

    while (1) {
        /* Wait for receive task to wakeup */
        sigwait(&lpc_enetif->rxset, &sig);
        LWIP_DEBUGF(DRIVER_DEBUG, ("+++++++++++ RX SIGNAL RECEIVED\n"));
        if(sig != RX_SIGNAL) {
            continue;
        }

        /* Process packets until all empty */
        while (LPC_EMAC->RxConsumeIndex != LPC_EMAC->RxProduceIndex)
            lpc_enetif_input(lpc_enetif->netif);
    }
}

/** \brief  Transmit cleanup task
 *
 * This task is called when a transmit interrupt occurs and
 * reclaims the pbuf and descriptor used for the packet once
 * the packet has been transferred.
 *
 *  \param[in] pvParameters Not used yet
 */
static void packet_tx(void* pvParameters) {
    struct lpc_enetdata *lpc_enetif = pvParameters;
    s32_t idx;

    while (1) {
        /* Wait for transmit cleanup task to wakeup */
        sem_wait(&lpc_enetif->TxCleanSem);
        LWIP_DEBUGF(DRIVER_DEBUG, ("+++++++++++ TX SIGNAL RECEIVED\n"));

        /* Error handling for TX underruns. This should never happen unless
           something is holding the bus or the clocks are going too slow. It
           can probably be safely removed. */
        if (LPC_EMAC->IntStatus & EMAC_INT_TX_UNDERRUN) {
            LINK_STATS_INC(link.err);
            LINK_STATS_INC(link.drop);

            /* Get exclusive access */
            sys_mutex_lock(&lpc_enetif->TXLockMutex);
            /* Reset the TX side */
            LPC_EMAC->MAC1 |= EMAC_MAC1_RES_TX;
            LPC_EMAC->IntClear = EMAC_INT_TX_UNDERRUN;

            /* De-allocate all queued TX pbufs */
            for (idx = 0; idx < LPC_NUM_BUFF_TXDESCS; idx++) {
                if (lpc_enetif->txb[idx] != NULL) {
                    pbuf_free(lpc_enetif->txb[idx]);
                    lpc_enetif->txb[idx] = NULL;
                }
            }

            /* Restore access */
            sys_mutex_unlock(&lpc_enetif->TXLockMutex);
            /* Start TX side again */
            lpc_tx_setup(lpc_enetif);
        } else {
            /* Free TX buffers that are done sending */
            lpc_tx_reclaim(lpc_enetif->netif);
        }
    }
}

/** \brief  Low level init of the MAC and PHY.
 *
 *  \param[in]      netif  Pointer to LWIP netif structure
 */
static err_t low_level_init(struct netif *netif)
{
    struct lpc_enetdata *lpc_enetif = netif->state;
    err_t err = ERR_OK;

    /* Enable MII clocking */
    LPC_SC->PCONP |= CLKPWR_PCONP_PCENET;

    LPC_PINCON->PINSEL2 = 0x50150105;                  /* Enable P1 Ethernet Pins. */
    LPC_PINCON->PINSEL3 = (LPC_PINCON->PINSEL3 & ~0x0000000F) | 0x00000005;

    /* Reset all MAC logic */
    LPC_EMAC->MAC1 = EMAC_MAC1_RES_TX | EMAC_MAC1_RES_MCS_TX |
        EMAC_MAC1_RES_RX | EMAC_MAC1_RES_MCS_RX | EMAC_MAC1_SIM_RES |
        EMAC_MAC1_SOFT_RES;
    LPC_EMAC->Command = EMAC_CR_REG_RES | EMAC_CR_TX_RES | EMAC_CR_RX_RES |
        EMAC_CR_PASS_RUNT_FRM;
    usleep(10000);

    /* Initial MAC initialization */
    LPC_EMAC->MAC1 = EMAC_MAC1_PASS_ALL;
    LPC_EMAC->MAC2 = EMAC_MAC2_CRC_EN | EMAC_MAC2_PAD_EN |
        EMAC_MAC2_VLAN_PAD_EN;
    LPC_EMAC->MAXF = EMAC_ETH_MAX_FLEN;

    /* Set RMII management clock rate to lowest speed */
    LPC_EMAC->MCFG = EMAC_MCFG_CLK_SEL(11) | EMAC_MCFG_RES_MII;
    LPC_EMAC->MCFG &= ~EMAC_MCFG_RES_MII;

    /* Maximum number of retries, 0x37 collision window, gap */
    LPC_EMAC->CLRT = EMAC_CLRT_DEF;
    LPC_EMAC->IPGR = EMAC_IPGR_P1_DEF | EMAC_IPGR_P2_DEF;

#if LPC_EMAC_RMII
    /* RMII setup */
    LPC_EMAC->Command = EMAC_CR_PASS_RUNT_FRM | EMAC_CR_RMII;
#else
    /* MII setup */
    LPC_EMAC->CR = EMAC_CR_PASS_RUNT_FRM;
#endif

    /* Initialize the PHY and reset */
    err = lpc_phy_init(netif, LPC_EMAC_RMII);
    if (err != ERR_OK)
        return err;

    /* Save station address */
    LPC_EMAC->SA2 = (u32_t) netif->hwaddr[0] |
        (((u32_t) netif->hwaddr[1]) << 8);
    LPC_EMAC->SA1 = (u32_t) netif->hwaddr[2] |
        (((u32_t) netif->hwaddr[3]) << 8);
    LPC_EMAC->SA0 = (u32_t) netif->hwaddr[4] |
        (((u32_t) netif->hwaddr[5]) << 8);

    /* Setup transmit and receive descriptors */
    if (lpc_tx_setup(lpc_enetif) != ERR_OK)
        return ERR_BUF;
    if (lpc_rx_setup(lpc_enetif) != ERR_OK)
        return ERR_BUF;

    /* Enable packet reception */
#if IP_SOF_BROADCAST_RECV
    LPC_EMAC->RxFilterCtrl = EMAC_RFC_PERFECT_EN | EMAC_RFC_BCAST_EN | EMAC_RFC_MCAST_EN;
#else
    LPC_EMAC->RxFilterCtrl = EMAC_RFC_PERFECT_EN;
#endif

    /* Clear and enable rx/tx interrupts */
    LPC_EMAC->IntClear = 0xFFFF;
    LPC_EMAC->IntEnable = RXINTGROUP | TXINTGROUP;

    /* Enable RX and TX */
    LPC_EMAC->Command |= EMAC_CR_RX_EN | EMAC_CR_TX_EN;
    LPC_EMAC->MAC1 |= EMAC_MAC1_REC_EN;

    return err;
}

/* This function provides a method for the PHY to setup the EMAC
   for the PHY negotiated duplex mode */
void lpc_emac_set_duplex(int full_duplex)
{
    if (full_duplex) {
        LPC_EMAC->MAC2    |= EMAC_MAC2_FULL_DUP;
        LPC_EMAC->Command |= EMAC_CR_FULL_DUP;
        LPC_EMAC->IPGT     = EMAC_IPGT_FULL_DUP;
    } else {
        LPC_EMAC->MAC2    &= ~EMAC_MAC2_FULL_DUP;
        LPC_EMAC->Command &= ~EMAC_CR_FULL_DUP;
        LPC_EMAC->IPGT = EMAC_IPGT_HALF_DUP;
    }
}

/* This function provides a method for the PHY to setup the EMAC
   for the PHY negotiated bit rate */
void lpc_emac_set_speed(int mbs_100)
{
    if (mbs_100)
        LPC_EMAC->SUPP = EMAC_SUPP_SPEED;
    else
        LPC_EMAC->SUPP = 0;
}

/**
 * This function is the ethernet packet send function. It calls
 * etharp_output after checking link status.
 *
 * \param[in] netif the lwip network interface structure for this lpc_enetif
 * \param[in] q Pointer to pbug to send
 * \param[in] ipaddr IP address
 * \return ERR_OK or error code
 */
err_t lpc_etharp_output(struct netif *netif, struct pbuf *q,
                        ip_addr_t *ipaddr)
{
    /* Only send packet is link is up */
    if (netif->flags & NETIF_FLAG_LINK_UP)
        return etharp_output(netif, q, ipaddr);

    return ERR_CONN;
}

/* periodic PHY status update */
static void phy_update(void* args) {
    (void)args;
    while(1) {
        usleep(2500);
        lpc_phy_sts_sm(lpc_enetdata.netif);
    }
}

/**
 * Should be called at the beginning of the program to set up the
 * network interface.
 *
 * This function should be passed as a parameter to netif_add().
 *
 * @param[in] netif the lwip network interface structure for this lpc_enetif
 * @return ERR_OK if the loopif is initialized
 *         ERR_MEM if private data couldn't be allocated
 *         any other err_t on error
 */
err_t eth_arch_enetif_init(struct netif *netif)
{
    err_t err;

    LWIP_ASSERT("netif != NULL", (netif != NULL));

    lpc_enetdata.netif = netif;

    /* set MAC hardware address */
    netif->hwaddr[0]  = 0x02;
    netif->hwaddr[1]  = 0x66;
    netif->hwaddr[2]  = 0x9b;
    netif->hwaddr[3]  = 0x54;
    netif->hwaddr[4]  = 0x24;
    netif->hwaddr[5]  = 0x00;
    netif->hwaddr_len = ETHARP_HWADDR_LEN;

    /* maximum transfer unit */
    netif->mtu = 1500;

    /* device capabilities */
    netif->flags = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_ETHERNET | NETIF_FLAG_IGMP;

    /* Initialize the hardware */
    netif->state = &lpc_enetdata;
    err = low_level_init(netif);
    if (err != ERR_OK)
        return err;

#if LWIP_NETIF_HOSTNAME
    /* Initialize interface hostname */
    netif->hostname = "lwiplpc";
#endif /* LWIP_NETIF_HOSTNAME */

    netif->name[0] = 'e';
    netif->name[1] = 'n';

    netif->output = lpc_etharp_output;
    netif->linkoutput = lpc_low_level_output;

    /* CMSIS-RTOS, start tasks */
    err = sys_sem_new(&lpc_enetdata.xTXDCountSem, LPC_NUM_BUFF_TXDESCS);
    LWIP_ASSERT("xTXDCountSem creation error", (err == ERR_OK));

    err = sys_mutex_new(&lpc_enetdata.TXLockMutex);
    LWIP_ASSERT("TXLockMutex creation error", (err == ERR_OK));

    sigemptyset(&lpc_enetdata.rxset);
    sigaddset(&lpc_enetdata.rxset, RX_SIGNAL);
    LWIP_ASSERT("Could not create RX sigmask\n",
                pthread_sigmask(SIG_BLOCK, &lpc_enetdata.rxset, NULL) == 0);

    /* Packet receive task */
    lpc_enetdata.RxThread = sys_thread_new("receive_thread", packet_rx, netif->state, LPC_RX_THREAD_STACKSIZE, RX_PRIORITY);
    LWIP_ASSERT("RxThread creation error", (lpc_enetdata.RxThread));

    /* Transmit cleanup task */
    sem_init(&lpc_enetdata.TxCleanSem, 0, 0);
    /*LWIP_ASSERT("TxCleanSem creation error", (err == ERR_OK));*/
    lpc_enetdata.TxThread = sys_thread_new("txclean_thread", packet_tx, netif->state, LPC_TX_THREAD_STACKSIZE, TX_PRIORITY);

    /* periodic PHY status update */
    sys_thread_new("phy_update", phy_update, NULL, LPC_PHY_UPDATE_STACKSIZE, 0);

    return ERR_OK;
}

void eth_arch_enable_interrupts(void) {
    rtems_interrupt_handler_install(ENET_IRQn, "eth_interrupt", RTEMS_INTERRUPT_UNIQUE, ENET_IRQHandler, NULL);
}

void eth_arch_disable_interrupts(void) {
    rtems_interrupt_handler_remove(ENET_IRQn, ENET_IRQHandler, NULL);
}

/* #define DUMP_LPC_ENETDATA */
/* #define DUMP_LPC_ENETDATA_PRXS */
/* #define DUMP_LPC_ENETDATA_PRXD */
/* #define DUMP_LPC_ENETDATA_RXB */
/* #define DUMP_LPC_ENETDATA_PTXS */
/* #define DUMP_LPC_ENETDATA_PTXD */
/* #define DUMP_LPC_ENETDATA_TXB */
/* #define DUMP_LPC_ENETDATA_NETIF */
/* #define DUMP_LPC_ENETDATA_INDEX */
/* #define DUMP_LPC_EMAC */
/* #define DUMP_LWIP_STATS */

#ifdef DUMP_DRIVER_INFO
static void dump_driver_info(void)
{
    uint i;

    (void)i;
    sleep(1);
    printf("========================================================\n");
#ifdef DUMP_LPC_ENETDATA
    printf("---- lpc_enetdata\n");
    printf("-- &lpc_enetdata: %p\n", (void*)&lpc_enetdata);
    printf("-- sizeof(lpc_enetdata): %u\n", sizeof(lpc_enetdata));
    printf("-- &lpc_enetdata+sizeof(lpc_enetdata): 0x%x\n", ((uint)&lpc_enetdata+sizeof(lpc_enetdata)));
    printf("\n");
#endif /* DUMP_LPC_ENETDATA */

#ifdef DUMP_LPC_ENETDATA_PRXS
    printf("---- lpc_enetdata.prxs\n");
    printf("-- &lpc_enetdata.prxs: %p\n", (void*)&lpc_enetdata.prxs);
    printf("-- sizeof(lpc_enetdata.prxs): %u\n", sizeof(lpc_enetdata.prxs));
    printf("-- sizeof(LPC_TXRX_STATUS_T): %u\n", sizeof(LPC_TXRX_STATUS_T));
    printf("-- LPC_NUM_BUFF_RXDESCS: %u\n", LPC_NUM_BUFF_RXDESCS);
    printf("-- &lpc_enetdata.prxs+sizeof(lpc_enetdata.prxs): 0x%x\n", ((uint)&lpc_enetdata.prxs+sizeof(lpc_enetdata.prxs)));
    for(i=0 ; i<LPC_NUM_BUFF_RXDESCS ; ++i) {
        printf("-- &lpc_enetdata.prxs[%u]: %p\n", i, (void*)&lpc_enetdata.prxs[i]);
        printf("-- lpc_enetdata.prxs[%u].statusinfo: %u\n", i, lpc_enetdata.prxs[i].statusinfo);
        printf("-- lpc_enetdata.prxs[%u].statushashcrc: %u\n", i, lpc_enetdata.prxs[i].statushashcrc);
    }
    printf("\n");
#endif /* DUMP_LPC_ENETDATA_PRXS */

#ifdef DUMP_LPC_ENETDATA_PRXD
    printf("---- lpc_enetdata.prxd\n");
    printf("-- &lpc_enetdata.prxd: %p\n", (void*)&lpc_enetdata.prxd);
    printf("-- sizeof(lpc_enetdata.prxd): %u\n", sizeof(lpc_enetdata.prxd));
    printf("-- sizeof(LPC_TXRX_DESC_T): %u\n", sizeof(LPC_TXRX_DESC_T));
    printf("-- LPC_NUM_BUFF_RXDESCS: %u\n", LPC_NUM_BUFF_RXDESCS);
    printf("-- &lpc_enetdata.prxd+sizeof(lpc_enetdata.prxd): 0x%x\n", ((uint)&lpc_enetdata.prxd+sizeof(lpc_enetdata.prxd)));
    for(i=0 ; i<LPC_NUM_BUFF_RXDESCS ; ++i) {
        printf("-- &lpc_enetdata.prxd[%u]: %p\n", i, (void*)&lpc_enetdata.prxd[i]);
        printf("-- lpc_enetdata.prxd[%u].packet: 0x%x\n", i, lpc_enetdata.prxd[i].packet);
        printf("-- lpc_enetdata.prxd[%u].control: %u\n", i, lpc_enetdata.prxd[i].control);
    }
    printf("\n");
#endif /* DUMP_LPC_ENETDATA_PRXD */

#ifdef DUMP_LPC_ENETDATA_RXB
    printf("---- lpc_enetdata.rxb\n");
    printf("-- &lpc_enetdata.rxb: %p\n", (void*)&lpc_enetdata.rxb);
    printf("-- sizeof(lpc_enetdata.rxb): %u\n", sizeof(lpc_enetdata.rxb));
    printf("-- LPC_NUM_BUFF_RXDESCS: %u\n", LPC_NUM_BUFF_RXDESCS);
    printf("-- sizeof(struct pbuf): %u\n", sizeof(struct pbuf));
    printf("-- &lpc_enetdata.rxb+sizeof(lpc_enetdata.rxb): 0x%x\n", ((uint)&lpc_enetdata.rxb+sizeof(lpc_enetdata.rxb)));
    for(i=0 ; i<LPC_NUM_BUFF_RXDESCS ; ++i) {
        printf("-- &lpc_enetdata.rxb[%u]: %p\n", i, (void*)&lpc_enetdata.rxb[i]);
        printf("-- lpc_enetdata.rxb[%u]->next: %p\n", i, (void*)lpc_enetdata.rxb[i]->next);
        printf("-- lpc_enetdata.rxb[%u]->payload: %p\n", i, lpc_enetdata.rxb[i]->payload);
        printf("-- lpc_enetdata.rxb[%u]->tot_len: %u\n", i, lpc_enetdata.rxb[i]->tot_len);
        printf("-- lpc_enetdata.rxb[%u]->len: %u\n", i, lpc_enetdata.rxb[i]->len);
        printf("-- lpc_enetdata.rxb[%u]->type: %u\n", i, lpc_enetdata.rxb[i]->type);
        printf("-- lpc_enetdata.rxb[%u]->flags: %u\n", i, lpc_enetdata.rxb[i]->flags);
        printf("-- lpc_enetdata.rxb[%u]->ref: %u\n", i, lpc_enetdata.rxb[i]->ref);
    }
    printf("\n");
#endif /* DUMP_LPC_ENETDATA_RXB */

#ifdef DUMP_LPC_ENETDATA_PTXS
    printf("---- lpc_enetdata.ptxs\n");
    printf("-- &lpc_enetdata.ptxs: %p\n", (void*)&lpc_enetdata.ptxs);
    printf("-- sizeof(lpc_enetdata.ptxs): %u\n", sizeof(lpc_enetdata.ptxs));
    printf("-- sizeof(LPC_TXTX_STATUS_T): %u\n", sizeof(LPC_TXTX_STATUS_T));
    printf("-- LPC_NUM_BUFF_TXDESCS: %u\n", LPC_NUM_BUFF_TXDESCS);
    printf("-- &lpc_enetdata.ptxs+sizeof(lpc_enetdata.ptxs): 0x%x\n", ((uint)&lpc_enetdata.ptxs+sizeof(lpc_enetdata.ptxs)));
    for(i=0 ; i<LPC_NUM_BUFF_TXDESCS ; ++i) {
        printf("-- &lpc_enetdata.ptxs[%u]: %p\n", i, (void*)&lpc_enetdata.ptxs[i]);
        printf("-- lpc_enetdata.ptxs[%u].statusinfo: %u\n", i, lpc_enetdata.ptxs[i].statusinfo);
        printf("-- lpc_enetdata.ptxs[%u].statushashcrc: %u\n", i, lpc_enetdata.ptxs[i].statushashcrc);
    }
    printf("\n");
#endif /* DUMP_LPC_ENETDATA_PTXS */

#ifdef DUMP_LPC_ENETDATA_PTXD
    printf("---- lpc_enetdata.ptxd\n");
    printf("-- &lpc_enetdata.ptxd: %p\n", (void*)&lpc_enetdata.ptxd);
    printf("-- sizeof(lpc_enetdata.ptxd): %u\n", sizeof(lpc_enetdata.ptxd));
    printf("-- sizeof(LPC_TXTX_DESC_T): %u\n", sizeof(LPC_TXTX_DESC_T));
    printf("-- LPC_NUM_BUFF_TXDESCS: %u\n", LPC_NUM_BUFF_TXDESCS);
    printf("-- &lpc_enetdata.ptxd+sizeof(lpc_enetdata.ptxd): 0x%x\n", ((uint)&lpc_enetdata.ptxd+sizeof(lpc_enetdata.ptxd)));
    for(i=0 ; i<LPC_NUM_BUFF_TXDESCS ; ++i) {
        printf("-- &lpc_enetdata.ptxd[%u]: %p\n", i, (void*)&lpc_enetdata.ptxd[i]);
        printf("-- lpc_enetdata.ptxd[%u].packet: 0x%x\n", i, lpc_enetdata.ptxd[i].packet);
        printf("-- lpc_enetdata.ptxd[%u].control: %u\n", i, lpc_enetdata.ptxd[i].control);
    }
    printf("\n");
#endif /* DUMP_LPC_ENETDATA_PTXD */

#ifdef DUMP_LPC_ENETDATA_TXB
    printf("---- lpc_enetdata.txb\n");
    printf("-- &lpc_enetdata.txb: %p\n", (void*)&lpc_enetdata.txb);
    printf("-- sizeof(lpc_enetdata.txb): %u\n", sizeof(lpc_enetdata.txb));
    printf("-- LPC_NUM_BUFF_TXDESCS: %u\n", LPC_NUM_BUFF_TXDESCS);
    printf("-- sizeof(struct pbuf): %u\n", sizeof(struct pbuf));
    printf("-- &lpc_enetdata.txb+sizeof(lpc_enetdata.txb): 0x%x\n", ((uint)&lpc_enetdata.txb+sizeof(lpc_enetdata.txb)));
    for(i=0 ; i<LPC_NUM_BUFF_TXDESCS ; ++i) {
        printf("-- &lpc_enetdata.txb[%u]: %p\n", i, (void*)&lpc_enetdata.txb[i]);
        printf("-- lpc_enetdata.txb[%u]->next: %p\n", i, (void*)lpc_enetdata.txb[i]->next);
        printf("-- lpc_enetdata.txb[%u]->payload: %p\n", i, lpc_enetdata.txb[i]->payload);
        printf("-- lpc_enetdata.txb[%u]->tot_len: %u\n", i, lpc_enetdata.txb[i]->tot_len);
        printf("-- lpc_enetdata.txb[%u]->len: %u\n", i, lpc_enetdata.txb[i]->len);
        printf("-- lpc_enetdata.txb[%u]->type: %u\n", i, lpc_enetdata.txb[i]->type);
        printf("-- lpc_enetdata.txb[%u]->flags: %u\n", i, lpc_enetdata.txb[i]->flags);
        printf("-- lpc_enetdata.txb[%u]->ref: %u\n", i, lpc_enetdata.txb[i]->ref);
    }
    printf("\n");
#endif /* DUMP_LPC_ENETDATA_TXB */

#ifdef DUMP_LPC_ENETDATA_NETIF
    printf("---- lpc_enetdata.netif\n");
    printf("-- lpc_enetdata.netif: %p\n", (void*)lpc_enetdata.netif);
    printf("-- sizeof(*lpc_enetdata.netif): %u\n", sizeof(*lpc_enetdata.netif));
    printf("-- lpc_enetdata.netif->ip_addr: %s\n", ipaddr_ntoa(&lpc_enetdata.netif->ip_addr));
    printf("-- lpc_enetdata.netif->netmask: %s\n", ipaddr_ntoa(&lpc_enetdata.netif->netmask));
    printf("-- lpc_enetdata.netif->gw: %s\n", ipaddr_ntoa(&lpc_enetdata.netif->gw));
    printf("-- lpc_enetdata.netif->mtu: %u\n", lpc_enetdata.netif->mtu);
    printf("-- lpc_enetdata.netif->hwaddr_len: %u\n", lpc_enetdata.netif->hwaddr_len);
    printf("-- lpc_enetdata.netif->hwaddr: %02x:%02x:%02x:%02x:%02x:%02x\n",
           lpc_enetdata.netif->hwaddr[0], lpc_enetdata.netif->hwaddr[1],
           lpc_enetdata.netif->hwaddr[2], lpc_enetdata.netif->hwaddr[3],
           lpc_enetdata.netif->hwaddr[4], lpc_enetdata.netif->hwaddr[5]);
    printf("-- lpc_enetdata.netif->flags: %u\n", lpc_enetdata.netif->flags);
    printf("-- lpc_enetdata.netif->name: %s\n", lpc_enetdata.netif->name);
    printf("-- lpc_enetdata.netif->num: %u\n", lpc_enetdata.netif->num);
    printf("\n");
#endif /* DUMP_LPC_ENETDATA */

#ifdef DUMP_LPC_ENETDATA_INDEX_LPC_EMAC
    printf("---- lpc_enetdata indexes\n");
    printf("-- lpc_enetdata.rx_fill_desc_index: %u\n", lpc_enetdata.rx_fill_desc_index);
    printf("-- lpc_enetdata.rx_free_descs: %u\n", lpc_enetdata.rx_free_descs);
    printf("\n");
#endif /* DUMP_LPC_ENETDATA_INDEX */

#ifdef DUMP_LPC_EMAC
    /* printf("---- LPC_EMAC\n"); */
    printf("-- LPC_EMAC->MAC1: %lu\n", LPC_EMAC->MAC1);
    printf("-- LPC_EMAC->MAC2: %lu\n", LPC_EMAC->MAC2);
    printf("-- LPC_EMAC->IPGT: %lu\n", LPC_EMAC->IPGT);
    printf("-- LPC_EMAC->IPGR: %lu\n", LPC_EMAC->IPGR);
    printf("-- LPC_EMAC->CLRT: %lu\n", LPC_EMAC->CLRT);
    printf("-- LPC_EMAC->MAXF: %lu\n", LPC_EMAC->MAXF);
    printf("-- LPC_EMAC->SUPP: %lu\n", LPC_EMAC->SUPP);
    printf("-- LPC_EMAC->TEST: %lu\n", LPC_EMAC->TEST);
    printf("-- LPC_EMAC->MCFG: %lu\n", LPC_EMAC->MCFG);
    printf("-- LPC_EMAC->MCMD: %lu\n", LPC_EMAC->MCMD);
    printf("-- LPC_EMAC->MADR: %lu\n", LPC_EMAC->MADR);
    printf("-- LPC_EMAC->MRDD: %lu\n", LPC_EMAC->MRDD);
    printf("-- LPC_EMAC->MIND: %lu\n", LPC_EMAC->MIND);
    printf("-- LPC_EMAC->SA0: %lu\n", LPC_EMAC->SA0);
    printf("-- LPC_EMAC->SA1: %lu\n", LPC_EMAC->SA1);
    printf("-- LPC_EMAC->SA2: %lu\n", LPC_EMAC->SA2);
    printf("-- LPC_EMAC->Command: %lu\n", LPC_EMAC->Command);
    printf("-- LPC_EMAC->Status: %lu\n", LPC_EMAC->Status);
    printf("-- LPC_EMAC->RxDescriptor: 0x%x\n", (uint)LPC_EMAC->RxDescriptor);
    printf("-- LPC_EMAC->RxStatus: 0x%x\n", (uint)LPC_EMAC->RxStatus);
    printf("-- LPC_EMAC->RxDescriptorNumber: %lu\n", LPC_EMAC->RxDescriptorNumber);
    printf("-- LPC_EMAC->RxProduceIndex: %u\n", (uint)LPC_EMAC->RxProduceIndex);
    printf("-- LPC_EMAC->RxConsumeIndex: %u\n", (uint)LPC_EMAC->RxConsumeIndex);
    printf("-- LPC_EMAC->TxDescriptor: 0x%x\n", (uint)LPC_EMAC->TxDescriptor);
    printf("-- LPC_EMAC->TxStatus: 0x%x\n", (uint)LPC_EMAC->TxStatus);
    printf("-- LPC_EMAC->TxDescriptorNumber: %lu\n", LPC_EMAC->TxDescriptorNumber);
    printf("-- LPC_EMAC->TxProduceIndex: %u\n", (uint)LPC_EMAC->TxProduceIndex);
    printf("-- LPC_EMAC->TxConsumeIndex: %u\n", (uint)LPC_EMAC->TxConsumeIndex);
    printf("-- LPC_EMAC->TSV0: %lu\n", LPC_EMAC->TSV0);
    printf("-- LPC_EMAC->TSV1: %lu\n", LPC_EMAC->TSV1);
    printf("-- LPC_EMAC->RSV: %lu\n", LPC_EMAC->RSV);
    printf("-- LPC_EMAC->FlowControlCounter: %lu\n", LPC_EMAC->FlowControlCounter);
    printf("-- LPC_EMAC->FlowControlStatus: %lu\n", LPC_EMAC->FlowControlStatus);
    printf("-- LPC_EMAC->IntStatus: %lu\n", LPC_EMAC->IntStatus);
    printf("-- LPC_EMAC->IntEnable: %lu\n", LPC_EMAC->IntEnable);
    printf("-- LPC_EMAC->PowerDown: %lu\n", LPC_EMAC->PowerDown);
    printf("-- LPC_EMAC->Module_ID: %lu\n", LPC_EMAC->Module_ID);
#endif /* DUMP_LPC_EMAC */

#ifdef DUMP_LWIP_STATS
    TCP_STATS_DISPLAY();
    UDP_STATS_DISPLAY();
    ICMP_STATS_DISPLAY();
    IP_STATS_DISPLAY();
    IPFRAG_STATS_DISPLAY();
    ETHARP_STATS_DISPLAY();
    LINK_STATS_DISPLAY();
    MEM_STATS_DISPLAY();
    for (i = 0; i < MEMP_MAX; i++) {
        MEMP_STATS_DISPLAY(i);
    }
    SYS_STATS_DISPLAY();
#endif /* DUMP_LWIP_STATS */
    printf("========================================================\n");
}
#endif /* DUMP_DRIVER_INFO */
