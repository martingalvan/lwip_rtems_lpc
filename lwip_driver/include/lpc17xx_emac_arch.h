#ifndef _LPC17XX_EMAC_ARCH_H_
#define _LPC17XX_EMAC_ARCH_H_

#include "lwip/netif.h"

#ifdef __cplusplus
extern "C" {
#endif

void eth_arch_enable_interrupts(void);
void eth_arch_disable_interrupts(void);
err_t eth_arch_enetif_init(struct netif *netif);

#ifdef __cplusplus
}
#endif

#endif /* _LPC17XX_EMAC_ARCH_H_ */
