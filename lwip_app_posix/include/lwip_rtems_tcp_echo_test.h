#ifndef _LWIP_RTEMS_TCP_ECHO_TEST_H_
#define _LWIP_RTEMS_TCP_ECHO_TEST_H_

/**
 * Test initialization.
 */
void tcp_echo_test_init(void);

#endif /* _LWIP_RTEMS_TCP_ECHO_TEST_H_ */
