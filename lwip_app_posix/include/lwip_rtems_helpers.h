#ifndef _LWIP_RTEMS_HELPERS_H_
#define _LWIP_RTEMS_HELPERS_H_

#ifdef LWIP_DEBUG

#include "rtems/stackchk.h"
#include "rtems/malloc.h"
#include "lwip/stats.h"

/**
 * Dump LWIP memory statistics.
 */
#define DUMP_MEM() {                                                    \
        LWIP_PLATFORM_DIAG(("#### START - DUMP LWIP HEAP: %s\n", __PRETTY_FUNCTION__)); \
        MEM_STATS_DISPLAY();                                            \
        LWIP_PLATFORM_DIAG(("#### END - DUMP LWIP HEAP\n"));              \
    }

/**
 * Dump LWIP sys usage statistics.
 */
#define DUMP_SYS() {                                                    \
        LWIP_PLATFORM_DIAG(("#### START - DUMP LWIP SYS: %s\n", __PRETTY_FUNCTION__)); \
        SYS_STATS_DISPLAY();                                            \
        LWIP_PLATFORM_DIAG(("#### END - DUMP LWIP SYS\n"));               \
    }

/**
 * Dump stack usage.
 */
#define DUMP_STACK() {                                                  \
        LWIP_PLATFORM_DIAG(("#### START - DUMP STACK: %s\n", __PRETTY_FUNCTION__)); \
        rtems_stack_checker_report_usage();                             \
        LWIP_PLATFORM_DIAG(("#### END - DUMP STACK\n"));                  \
    }

/**
 * Dump malloc statistics.
 */
#define DUMP_MALLOC() {                                                 \
        LWIP_PLATFORM_DIAG(("#### START - DUMP MALLOC: %s\n", __PRETTY_FUNCTION__)); \
        malloc_report_statistics();                                     \
        LWIP_PLATFORM_DIAG(("#### END - DUMP MALLOC\n"));                 \
    }

#else /* LWIP_DEBUG */

#define DUMP_MEM()
#define DUMP_SYS()
#define DUMP_STACK()
#define DUMP_MALLOC()

#endif /* LWIP_DEBUG */

#endif /* _LWIP_RTEMS_HELPERS_H_ */
