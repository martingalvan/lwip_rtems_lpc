#include "lwip/debug.h"
#include "lwip/sys.h"
#include "lwip/sockets.h"
#include "lwip/tcpip.h"
#include "lwip/netif.h"
#include "lpc17xx_emac_arch.h"
#include "lwip_rtems_helpers.h"
#include <unistd.h>

/**
 * Network interface configurations.
 */
#define NETIF_ADDR "192.168.0.10"
#define NETIF_MASK "255.255.255.0"
#define NETIF_NET  "192.168.0.0"
#define NETIF_GW   "192.168.0.1"

/**
 * Server configurations.
 */
#define SRV_HOST NETIF_ADDR
#define SRV_PORT 9999

/**
 * Main loop sleep time [ms].
 */
#define MAIN_LOOP_SLEEP (10*1000)

/**
 * Bytes to echo.
 */
#define BYTES_TO_ECHO 10

/**
 * Maximum length to which the queue of pending connections for sockfd may grow.
 */
#define LISTEN_BACKLOG 5

/**
 * Network interface.
 */
static struct netif nif;

/*----------------------------------------------------------------------------*/
/**
 * LWIP network interface status callback.
 * @param[in] netif Network interface container.
 */
static void status_callback(struct netif* netif)
{
    (void)netif;
    LWIP_DEBUGF(APP_DEBUG, (">> SRV - Network interface status changed: %u\n",
                            netif_is_up(netif)));
}
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 * LWIP network link status callback.
 * @param[in] netif Network interface container.
 */
static void link_callback(struct netif* netif)
{
    (void)netif;
    LWIP_DEBUGF(APP_DEBUG, (">> SRV - Network link status changed: %u\n",
                            netif_is_up(netif)));
}
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 * Start TCP server.
 * @param[in] args Thread arguments.
 */
static void tcp_echo_test_listen(void *args)
{
    int                connfd;
    int                listenfd;
    struct sockaddr_in srv_addr;
    struct sockaddr_in cli_addr;
    socklen_t          cli_len;
    char               cli_addr_str[INET_ADDRSTRLEN];
    char               rxbuff[BYTES_TO_ECHO];
    ssize_t            rxlen;
    ssize_t            txlen;

    LWIP_UNUSED_ARG(args);

    /* Create listen socket. */
    LWIP_DEBUGF(APP_DEBUG, (">> SRV - Creating listen socket...\n"));
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    if(listenfd == -1) {
        LWIP_DEBUGF(APP_DEBUG, ("!! SRV - Could not create listen socket"));
        return;
    }

    /* Configure server address. */
    LWIP_DEBUGF(APP_DEBUG, (">> SRV - Configuring server address...\n"));
    memset(&srv_addr, 0, sizeof(srv_addr));
    srv_addr.sin_family      = AF_INET;
    srv_addr.sin_port        = htons(SRV_PORT);
    srv_addr.sin_addr.s_addr = INADDR_ANY;

    /* Bind socket. */
    LWIP_DEBUGF(APP_DEBUG, (">> SRV - Binding listen socket...\n"));
    if(bind(listenfd, (struct sockaddr*)&srv_addr, sizeof(srv_addr)) == -1) {
        LWIP_DEBUGF(APP_DEBUG, ("!! SRV - Could not bind listen socket"));
        close(listenfd);
        return;
    }

    /* Start listening. */
    LWIP_DEBUGF(APP_DEBUG, (">> SRV - Starting to listen...\n"));
    if(listen(listenfd, LISTEN_BACKLOG) == -1) {
        LWIP_DEBUGF(APP_DEBUG, ("!! SRV - Could not start listening"));
        close(listenfd);
        return;
    }

    /* Handle incomming packages. */
    for(;;) {
        /* Accept connections. */
        LWIP_DEBUGF(APP_DEBUG, (">> SRV - Accepting new connection...\n"));
        cli_len = sizeof(cli_addr);
        connfd  = accept(listenfd, (struct sockaddr*)&cli_addr, &cli_len);
        if(connfd == -1) {
            LWIP_DEBUGF(APP_DEBUG, ("!! SRV - Could not get connection"));
            continue;
        }

        /* Get connection data. */
        inet_ntop(AF_INET, (ip_addr_t*)&cli_addr.sin_addr,
                  cli_addr_str, sizeof(cli_addr_str));
        LWIP_DEBUGF(APP_DEBUG, (">> SRV - Connection established: %s:%d\n",
                                cli_addr_str, ntohs(cli_addr.sin_port)));

        /* Read message. */
        LWIP_DEBUGF(APP_DEBUG, (">> SRV - Trying to read echo message...\n"));
        rxlen = read(connfd, rxbuff, BYTES_TO_ECHO);
        if(rxlen != BYTES_TO_ECHO) {
            LWIP_DEBUGF(APP_DEBUG, ("!! SRV - Could not read message"));
            close(connfd);
            continue;
        }
        LWIP_DEBUGF(APP_DEBUG, (">> SRV - READ [%u]: %s\n",
                                (uint)rxlen, rxbuff));

        /* Echo message. */
        LWIP_DEBUGF(APP_DEBUG, (">> SRV - Trying to write echo message...\n"));
        txlen = write(connfd, rxbuff, BYTES_TO_ECHO);
        if(txlen != BYTES_TO_ECHO) {
            LWIP_DEBUGF(APP_DEBUG, ("!! Could not echo message"));
            close(connfd);
            continue;
        }
        LWIP_DEBUGF(APP_DEBUG, (">> SRV - WRITE [%u]: %s\n",
                                (uint)txlen, rxbuff));

        /* Close connection. */
        close(connfd);
    }

    /* Stop listening. */
    close(listenfd);
}
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 * Configure the primary network interface.
 * @param[in] args Semphore used in the initialization.
 */
static void tcp_echo_test_init_netif(void* args)
{
    sys_sem_t* sem = NULL;
    ip_addr_t  ipaddr;
    ip_addr_t  netmask;
    ip_addr_t  gateway;

    /* Get semaphore. */
    sem = (sys_sem_t*)args;

    /* Setup. */
    LWIP_DEBUGF(APP_DEBUG, (">> SRV - Initializing network interface: "
                            "[Address:%s] [Netmask:%s] [Network:%s] "
                            "[Gateway:%s]\n",
                            NETIF_ADDR, NETIF_MASK, NETIF_NET, NETIF_GW));
    memset(&nif, 0, sizeof(nif));
    ipaddr.addr  = ipaddr_addr(NETIF_ADDR);
    netmask.addr = ipaddr_addr(NETIF_MASK);
    gateway.addr = ipaddr_addr(NETIF_GW);
    if(netif_add(&nif, &ipaddr, &netmask, &gateway, NULL, eth_arch_enetif_init,
                 tcpip_input) == NULL) {
        LWIP_DEBUGF(APP_DEBUG, ("!! SRV - Could not add netif"));
        return;
    }
    netif_set_default(&nif);
    netif_set_status_callback(&nif, status_callback);
    netif_set_link_callback(&nif, link_callback);
    netif_set_up(&nif);
    LWIP_DEBUGF(APP_DEBUG, (">> SRV - Initialize network interface done\n"));

    /* Continue. */
    sys_sem_signal(sem);
}
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 * Test initialization.
 */
void tcp_echo_test_init(void)
{
    sys_sem_t sem;

    /* Initialize network stack. */
    LWIP_DEBUGF(APP_DEBUG, (">> SRV - Initializing network stack...\n"));
    if(sys_sem_new(&sem, 0) != ERR_OK) {
        LWIP_DEBUGF(APP_DEBUG, ("!! SRV - Could not initialize network stack\n"));
        return;
    }
    tcpip_init(tcp_echo_test_init_netif, &sem);
    sys_sem_wait(&sem);
    sys_sem_free(&sem);

    /* Enable eth interruptions. */
    eth_arch_enable_interrupts();

    /* Start listen. */
    tcp_echo_test_listen(NULL);

    /* Main loop. Break it only when a critical error occurred. */
    while(1) {
        usleep(MAIN_LOOP_SLEEP);
    }
}
/*----------------------------------------------------------------------------*/
